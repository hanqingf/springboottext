package com.han;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

@SpringBootTest
class DemoApplicationTests {

  @Autowired
  DataSource dataSource;
  @Autowired
  JavaMailSender mailSender;

  @Test
  void contextLoads1() throws SQLException {

    System.out.println(dataSource.getClass());
    Connection connection = dataSource.getConnection();
    System.out.println(connection);
    connection.close();
  }
  @Test
  void contextLoads2(){
    SimpleMailMessage smm = new SimpleMailMessage();
    smm.setSubject("你好");
    smm.setText("你好啊");
    smm.setTo("23599975@qq.com");
    smm.setFrom("hanqingfeng002@ke.com");
    mailSender.send(smm);
  }
  @Test
  void contextLoads3(){
    mailSender.createMimeMessage();
  }


}
