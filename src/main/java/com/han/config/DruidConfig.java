package com.han.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
public class DruidConfig {
  @Bean
  @ConfigurationProperties(prefix = "spring.datasource")
  public DataSource druidDataSource() {
    return new DruidDataSource();
  }

  //后台监控
  @Bean
  public ServletRegistrationBean statViewServlet() {
    ServletRegistrationBean<StatViewServlet> bean = new ServletRegistrationBean<>(new StatViewServlet(), "/druid/*");
    //设置账号密码
    HashMap<String, String> initParameters = new HashMap<>();
    //设置账号密码
    initParameters.put("loginUsername", "admin");
    initParameters.put("loginPassword", "123456");
    //允许谁可以访问
    initParameters.put("allow", "");
    //禁止访问
//    initParameters.put("","192.168.X.X");
    bean.setInitParameters(initParameters);
    return bean;
  }


  //filter
  public FilterRegistrationBean webStatFilter() {
    FilterRegistrationBean bean = new FilterRegistrationBean<>();
    bean.setFilter(new WebStatFilter());
    HashMap<String, String> initParameters = new HashMap<>();
    //这些定向
    initParameters.put("exclusions", "*.js,*.css,/druid/*");

    bean.setInitParameters(initParameters);
    return bean;
  }
}
