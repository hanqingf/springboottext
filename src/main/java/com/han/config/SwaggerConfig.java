package com.han.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

  private ApiInfo apiInfo(){
    Contact han = new Contact("han", "http://120.1.1.1", "234@qq.com");
    return new ApiInfo("我的SWAGGER",
        "帅", "1.0",
        "urn:tos",
         han,
        "Apache 2.0",
        "http://www.apache.org/licenses/LICENSE-2.0",
        new ArrayList());
  }

  @Bean
  public Docket getDocket(Environment environment) {
    //获取项目的环境
    Profiles profiles = Profiles.of("dev", "test");
    //判断是否为测试或者开发环境
    boolean flag = environment.acceptsProfiles(profiles);

    return new Docket(DocumentationType.SWAGGER_2)
        .apiInfo(apiInfo())
        .groupName("han")
        .select()
        //any()扫描全部
        //basePackage指定包
        //none()不扫描
        .apis(RequestHandlerSelectors.basePackage("com.han.controller"))
        //过滤 ant只扫描这个下面的
        .paths(PathSelectors.any())
        .build()
        .enable(flag);
  }
}
