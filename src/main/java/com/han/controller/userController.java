package com.han.controller;

import com.han.mapper.UserMapper;
import com.han.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class userController {
  @Autowired
  private UserMapper userMapper;

  @GetMapping("/queryUserList")
  public List<User> queryUserList() {
    List<User> users = userMapper.queryUserList();
    return users;
  }
}
