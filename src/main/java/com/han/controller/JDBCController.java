package com.han.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class JDBCController {

  @Autowired
  JdbcTemplate jdbcTemplate;

  @GetMapping("/userList")
  public List<Map<String, Object>> userList() {
    String sql = "select * from user";
    List<Map<String, Object>> list_maps = jdbcTemplate.queryForList(sql);
    return list_maps;
  }

  @GetMapping("/addUser")
  public String addUser() {
    String sql = "insert into user(id,name,password) values(5,'小明','aaa')";
    jdbcTemplate.update(sql);
    return "addUser ok";
  }

  @GetMapping("/updateUser/{id}")
  public String updateUser(@PathVariable("id") int id) {
    String sql = "update user set name=?,password=?  where id =" + id;
    Object[] objects = new Object[2];
    objects[0] = "小红";
    objects[1] = "zzzz";

    jdbcTemplate.update(sql, objects);
    return "updateUser ok";
  }

  @GetMapping("/deleteUser/{id}")
  public String deleteUser(@PathVariable("id") int id) {
    String sql = "delete from user where id=" + id;


    jdbcTemplate.update(sql);
    return "deleteUser ok";
  }

}
