package com.han.controller;

import com.han.pojo.User;
import io.swagger.annotations.ApiParam;
import org.springframework.scheduling.annotation.Async;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class SwaggerController {
  //开启异步
  @Async
  @PostMapping("/hi")
  public User hello(@ApiParam("用户名") User user){
    return user;
  }

  @GetMapping("/h0")
  public String hello0(){
    return "h0";
  }
}
